.. py_mini_sh documentation.
   Copyright Abelana Ltd, 2018.
   Licensed under the terms of the MIT License. See LICENSE.txt.

.. _defines_list:

Built-in Definitions
====================

The following table defines all the variables that are
pre-defined. Values passed into the :py:func:`py_mini_sh.run` function
are added to this list of built-in values (and override them).

+-----------------+----------------------------------+------------------------------------------+
| Name            |  Value                           | Comment                                  |
+=================+==================================+==========================================+
| PYVER           | e.g ``'2.7'``                    | Current Python major + minor version.    |
+-----------------+----------------------------------+------------------------------------------+
| PYVER_NUMBER    | e.g. ``'27'``                    | Current Python major and minor version.  |
+-----------------+----------------------------------+------------------------------------------+
| BIN_DIR         | On Windows ``'Scripts'``         | On Linux it will be ``'bin'``.           |
+-----------------+----------------------------------+------------------------------------------+
| SEP             | On Windows ``'\\'``              | On Linux it will be ``'/'``.             |
+-----------------+----------------------------------+------------------------------------------+
| PYTHON_EXEC     | ``sys.executable``               | Full path to the running interpreter     |
+-----------------+----------------------------------+------------------------------------------+
| VENV_BIN        | ``sys.executable`` folder        | The folder in which the running          |
|                 |                                  | intepreter is located.                   |
+-----------------+----------------------------------+------------------------------------------+
| SITE_PACKAGES   | Platform dependent               | The site-packages folder for the current |
|                 |                                  | interpreter.                             |
+-----------------+----------------------------------+------------------------------------------+
| PLATFORM        | On Windows ``'win'``             | On Linux ``'linux'``.                    |
+-----------------+----------------------------------+------------------------------------------+
| ALLVARIANTS     | ``'{PLATFORM}-{PYVER_NUMBER}'``  | Useful for specifying varianted build or |
|                 |                                  | ship folders.                            |
+-----------------+----------------------------------+------------------------------------------+
| PYPI_HOST       | From environment                 | Pulled from the env var if it's there.   |
+-----------------+----------------------------------+------------------------------------------+
| PIP_FLAGS       | ``'-i http://{PYPI_HOST}/simple  | Or is set to ``''`` if PYPI_HOST is      |
|                 | --trusted-host={PYPI_HOST}'``    | not defined.                             |
+-----------------+----------------------------------+------------------------------------------+
| OS              | Value dependent on the running   | Examples are ``windows_10``              |
|                 | platform                         | ``ubuntu_16.04``, ``ubuntu_20.04``.      |
+-----------------+----------------------------------+------------------------------------------+

.. _code_docs:


Code Documentation
==================

.. automodule:: py_mini_sh
    :members:
