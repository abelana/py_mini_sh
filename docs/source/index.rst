.. py_mini_sh documentation master file
   Copyright Abelana Ltd, 2018.
   Licensed under the terms of the MIT License. See LICENSE.txt.


Package py_mini_sh
==================


.. toctree::
   :maxdepth: 2

   overview
   codedoc
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

