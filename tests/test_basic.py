"""
pytest test for the py_mini_sh package.
"""

#
# Copyright Abelana Ltd, 2018.
# Licensed under the terms of the MIT License. See LICENSE.txt.
#

import re
import pytest

import py_mini_sh

@pytest.fixture(scope='function')
def clear_defines():
    """This fixture ensures the global DEFINES is restored to its initial
    state after each test.
    """
    safe = py_mini_sh.DEFINES
    yield
    py_mini_sh.DEFINES = safe


def test_exports():
    assert py_mini_sh.run and py_mini_sh.expand and \
        py_mini_sh.DEFINES, "The imort failed to pull things in"


def test_run_1(clear_defines):
    def fun():
        assert py_mini_sh.expand('{FOO}') == 'foo'
    py_mini_sh.run(fun, dict(FOO='foo'))


def test_run_2(clear_defines):
    def fun():
        assert py_mini_sh.expand('{FOO}') == 'bar'
    py_mini_sh.run(fun, dict(FOO='{BAR}', BAR='bar'))
    

def test_exec_1(clear_defines):
    def fun():
        py_mini_sh.exec_('{LS} {LSARGS}')

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_exec_2(clear_defines, tmpdir):
    def fun():
        py_mini_sh.exec_('{LS} {LSARGS}', cwd=str(tmpdir))

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_exec_3(clear_defines):
    def fun():
        py_mini_sh.exec_('{LS} {LSARGS} doesnotexist')

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 1


def test_pipe_1(clear_defines):
    def fun():
        stdout = py_mini_sh.pipe('{LS} {LSARGS}')
        assert len(stdout) > 4

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_pipe_2(clear_defines, tmpdir):
    def fun():
        stdout = py_mini_sh.pipe('{LS} {LSARGS}', cwd=str(tmpdir))
        assert len(stdout) > 4

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_pipe_3(clear_defines):
    def fun():
        stdout = py_mini_sh.pipe('{LS} {LSARGS} doesnotexist')

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 1


def test_pipe_4(clear_defines):
    def fun():
        stdout = py_mini_sh.pipe('{LS} {LSARGS} doesnotexist', ignore_error=True)

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_pipe_5(clear_defines):
    def fun():
        if py_mini_sh.PY2:
            match_str = r'README\.\w\w\w'
        else:
            match_str = r'README\.\w\w\w'.encode('utf-8')
        reg = re.compile(match_str)
        match = py_mini_sh.pipe('{LS} {LSARGS}', regexp=reg)
        assert match.group()

    if py_mini_sh.WINDOWS:
        defs = dict(LS='dir', LSARGS='')
    else:
        defs = dict(LS='ls', LSARGS='-Fs')
        
    assert py_mini_sh.run(fun, defs) == 0


def test_download_1(clear_defines, tmpdir):
    def fun():
        into = str(tmpdir) + '{SEP}{FNAME}'
        py_mini_sh.download('{PYTHON_HOME}', into)
        assert py_mini_sh.is_f(into)
       
    assert py_mini_sh.run(fun, dict(PYTHON_HOME='https://google.com',
                                    FNAME='google-home-page.html')) == 0


def test_download_2(clear_defines, tmpdir):
    def fun():
        into = str(tmpdir) + '{SEP}{FNAME}'
        download = str(tmpdir) + '{SEP}download'
        py_mini_sh.download('{EGG_URL}', into, unpack=True, unpack_dir=download)
        assert py_mini_sh.is_d(download)
        assert py_mini_sh.is_d(download + '{SEP}pytest-sphinx-0.2')
        assert not py_mini_sh.is_f(str(tmpdir) + '{SEP}{FNAME}')
       
    assert py_mini_sh.run(fun, dict(EGG_URL='https://files.pythonhosted.org/packages/8f/7f/efa5ddd054fee3fec3e276ae9aa976fa1be51faf08544c5ceb1457882781/pytest-sphinx-0.2.tar.gz',
                                    FNAME='pytest-sphinx-0.2.tar.gz')) == 0


def test_download_3(clear_defines, tmpdir):
    def fun():
        into = str(tmpdir) + '{SEP}{FNAME}'
        py_mini_sh.download('{URL}', into)
        assert py_mini_sh.is_f(into)

    if py_mini_sh.PY2:
        # On Python 2.7 the urlretrieve still succeeds on a 404 error.
        expected = 0
    else:
        expected = 1
        
    assert py_mini_sh.run(fun, dict(URL='http://google.com/thisdoesntexist',
                                    FNAME='nothing-to-see-here.html')) == expected


def test_readall_1(clear_defines):
    def fun():
        contents = py_mini_sh.readall('{INPUT_FILE}')
        assert len(contents) > 20

    assert py_mini_sh.run(fun, dict(INPUT_FILE='README.rst')) == 0


def test_writeall_1(clear_defines, tmpdir):
    def fun():
        contents = py_mini_sh.readall('{INPUT_FILE}')
        outf = str(tmpdir) + '{SEP}{OUTPUT_FILE}' 
        py_mini_sh.writeall(outf, ''.join(contents))
        assert py_mini_sh.is_f(outf)
        py_mini_sh.del_(outf)
        assert not py_mini_sh.is_f(outf)

    assert py_mini_sh.run(fun, dict(INPUT_FILE='README.rst',
                                    OUTPUT_FILE='Copy_README.rst')) == 0


def test_parse_pylint_1(clear_defines):
    def fun():
        contents = py_mini_sh.readall('{INPUT_FILE}')
        numbers = py_mini_sh.parse_pylint_output(contents)
        assert len(numbers) == 4

    assert py_mini_sh.run(fun, dict(INPUT_FILE='py_mini_sh-pylint')) == 0


def test_copytree_1(clear_defines, tmpdir):
    def fun():
        output_dir = str(tmpdir) + '{SEP}{INPUT_DIR}'
        py_mini_sh.copytree('{INPUT_DIR}', output_dir)
        assert py_mini_sh.is_d(output_dir)
        
    assert py_mini_sh.run(fun, dict(INPUT_DIR='py_mini_sh')) == 0
